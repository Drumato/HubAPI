require 'json'
require 'net/http'
require 'terminal-table'
require 'color_echo/get'


class HubAPI
  HOST = "https://api.github.com"
  attr_accessor :name

  def initialize(username)
    self.name = username
  end

  def action(method)
      self.send(method)
  end

  def getuserrepos
    colortable = {
      'Dockerfile'=>:cyan,
      'C'=>:yellow,
      'C++'=>:yellow,
      'Ruby'=>:red,
      'Python'=>:blue,
      'Jupyter Notebook'=>:magenta,
      'Golang'=>:h_blue,
      'Haskell'=>:h_green}
    changecolors = ->(language){ CE.fg(colortable[language]).get(language) }
    repos = send_request("#{HOST}/users/#{self.name}/repos",'get')
    names = []
    repos.each do |repo|
      names.append([repo['name'],changecolors[repo['language']],repo['html_url']])
    end
    names.sort!{|before,after| before[1] <=> after[1]}
    return make_table(info2d=names,title="#{self.name}'s repositories",header:['name','language','URL'])
  end

  def create_new_repo
    params = %w(name description gitignore_template)
    platforms = %w(Ruby Python Go C C++ Haskell Rails)
    postdatas = []
    params.each do |param|
      puts "リポジトリの#{param}情報を入力してください｡"
      datapart = gets.chomp 
      postdatas.append(datapart)
    end
    response = send_request("#{HOST}/user/repos",'post_form',data:params.zip(postdatas).to_h)
    abort(CE.fg(:red).get('ERR!リポジトリの作成ができませんでした')) unless response.code == 201
    title = response["full_name"]
    info2d = [["URL",response["html_url"]]]
    return make_table(info2d,title=title)
  end

  def getcommits
    puts '取得したいリポジトリ名を入力してください｡'
    repo = gets.chomp
    commits = send_request("#{HOST}/repos/#{self.name}/#{repo}/commits",'get')
    hashes = []
    puts '取得したいコミット数を入力してください｡'
    commitscount = gets.chomp.to_i
    abort(CE.fg(:red).get('ERR!コミット数の指定が不正です')) if commitscount > commits.length 
    commitscount.times do |idx|
      hashes.append(commits[idx]['sha'])
    end
    info2d = []
    hashes.each do |hash|
      commit = send_request("#{HOST}/repos/#{self.name}/#{repo}/git/commits/#{hash}",'get')
      info2d.append([
        commit['committer']['name'],
        commit['committer']['date'],
        commit['message']])
    end
    return make_table(
      info2d=info2d,
      title="#{repo}'s #{commitscount} commits",
      header:['commiter','message','date'])
  end

  def currentcommits()
    return unless Dir.exist?("./.git")
    logs = `git log -5`
    authors = Array.new
    dates = Array.new
    messages = Array.new
    logs.split("\n").each do |log|
      matchauthor = /Author:[ ](\w+)[ ]/.match(log)
      matchdate = /Date:[ ]+(\w+[ ]){,5}/.match(log)
      matchmsg = /[ ]{4,}([\w\s]+)/.match(log)
      authors.append(matchauthor[1]) unless matchauthor.nil?
      dates.append(matchdate[0][8..-1]) unless matchdate.nil?
      messages.append(matchmsg[1]) unless matchmsg.nil?
    end
    commits = Array.new
    5.times do |idx|
      commits.append([authors[idx],messages[idx],dates[idx]]) 
    end

    return make_table(info2d=commits,title='last 5 commits',header:['author','message','commit-date'])
  end

  def send_request(url,method,**params)
    if params
      response = Net::HTTP.send(method,URI.parse(url),params[:data])
    else
      response = Net::HTTP.send(method,URI.parse(url))
    end
    info = JSON.parse(response)
    return info
  end

  def make_table(info2d,title,**options)
    table = Terminal::Table.new(title:title,rows:info2d,headings:options[:header])
    return table
  end

end

def ui
  puts 'GitHubの管理ツールです｡'
  puts 'UserNameを入力してください｡'
  username = gets.chomp
  hub = HubAPI.new(username)
  loop do
    puts '利用する機能を選択してください｡終わる場合はexitを入力してください｡'
    puts hub.make_table(
      info2d=[
        ['リポジトリ一覧','repos'],
        ['カレントリポジトリコミット一覧','current'],
        ['任意のリポジトリコミット一覧','commits'],
        ['リポジトリ作成','create']
            ],
      title='利用する機能の値を入力',
      header:['機能名','入力値']
    )
    proctable = {
      'repos'=>:getuserrepos,
      'current'=>:currentcommits,
      'commits'=>:getcommits,
      'create'=>:create_new_repo
    }
    process = gets.chomp
    abort(CE.fg(:red).get('ERR! No such a function')) unless proctable.keys.include?(process)
    process = proctable[process]
    puts hub.action(process) 
  end
end



if __FILE__ == $0
  ui
end
